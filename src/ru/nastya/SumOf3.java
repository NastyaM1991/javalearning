package ru.nastya;

import java.util.Scanner;

public class SumOf3
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int a1;
        int a2;
        int a3;

        a1 = scanner.nextInt();
        a2 = scanner.nextInt();
        a3 = scanner.nextInt();

        if (a1 + a2 == a3 || a1 + a3 == a2 ||a2 + a3 == a1)
        {
            System.out.println("YES");
        }
        else
        {
            System.out.println("NO");
        }
    }
}
