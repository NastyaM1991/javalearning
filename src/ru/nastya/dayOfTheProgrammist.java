package ru.nastya;

import java.util.Scanner;

public class dayOfTheProgrammist
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int year;
        year = scanner.nextInt();
        if (year%400 == 0 || year%4 == 0 && year%100 != 0)
        {
            if (year<10)
            {
                System.out.println("12/09/000"+year);
            }
            else if (year>=10 && year<100)
            {
                System.out.println("12/09/00"+year);
            }
            else if (year>=100 && year<1000)
            {
                System.out.println("12/09/0"+year);
            }
            else if (year>=1000 && year<10000)
            {
                System.out.println("12/09/"+year);
            }
        }
        else {
            if (year<10)
            {
                System.out.println("13/09/000"+year);
            }
            else if (year>=10 && year<100)
            {
                System.out.println("13/09/00"+year);
            }
            else if (year>=100 && year<1000)
            {
                System.out.println("13/09/0"+year);
            }
            else if (year>=1000 && year<10000)
            {
                System.out.println("13/09/"+year);
            }
        }
    }
}
