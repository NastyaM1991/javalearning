package ru.nastya;

import java.util.Scanner;

public class Year
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int num;

        //System.out.println("Input number of the month");
        num = scanner.nextInt();

        if (num == 12 || num == 1 || num == 2)
        {
            System.out.println("Winter");
        }
        else if (num == 3 || num == 4 || num == 5)
        {
            System.out.println("Spring");
        }
        else if (num == 6 || num == 7 || num == 8)
        {
            System.out.println("Summer");
        }
        else if (num == 9 || num == 10 || num == 11)
        {
            System.out.println("Autumn");
        }
        else{
            System.out.println("Error");
        }
    }
}
